# car_selection_system

## Запуск проекта

- `python -m venv venv` - создать виртуальное окружение
- `venv\Scripts\activate` - войти в виртуальное окружение
- `pip install -r requirements.txt` - установить зависимости
- `python ./app/manage.py runserver` - запустить сервер для разработки на http://127.0.0.1:8000




* для создания приложения использовала команду `django-admin startproject app`